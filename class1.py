# в файле class1.py создать класс Hello с конструктором у которого будет атрибут name
# и создать класс HI который унаследован от Hello у которого есть магический метод str
# с выводом атрибута name
# в файле task импортировать свой класс, создать экземпляр и применить на него функцию
# tptint

class Hello:
    def __init__(self, name):
        self.name = name


class Hi(Hello):
    def __str__(self):
        return f'{self.name}'
